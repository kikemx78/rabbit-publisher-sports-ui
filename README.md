# RabbitMQ Publisher for Sports Betting UI

This program contains two classes :  `OutcomesFeedService` *./src/index.ts*  & `RabbitPublisher` *./src/RabbitPublisher/inex.ts*.

`OutcomesFeedService` handles a connection to our back-end feed (either *live* or  *prematch*) through websockets (*socket-io*) ; and maintains a data 'state'.

`Rabbit Publisher` is a message-broker Publisher (RabbitMQ) that consumes and broadcasts this data. Data is consumed by `RabbitWorkers` (that 'live' inside UI's Node instance) to finally be broadcasted to browser-clients.

The main goal of this program is to have 'a single `RabbitPublisher` serving multiple `RabbitWorkers`' (or one backend feed serving multiple clients).

The program is configured to start two independent instances: *live* and *prematch*. You can define to start either one or both through env variables (there is an env.sample file with instructions on how to accomplish this).

On the near future we can escale this under a micro-services approach.

NOTE:

There is a 'mock' under `worker` folder. This is only for testing/development purposes as the 'real' worker needs to 'live' inside `sports-betting-ui-2_0` instance.

## Set-up
You will first need to install RabbitMQ on your server or local machine.

Instructions on how to accomplish this on your local machine can be found here : https://bit.ly/2RxlZ0u

A quick start guide for a Debian server can be found here https://bit.ly/2C3RFoE

Start up commands (Debian) :
```
sudo service rabbitmq-server start
sudo service rabbitmq-server stop
```

Once you have started the rabbitmq-server you can start the Publisher. For this you will need to create an .env file (you can copy env.sample and modify variables at will). Once this is done you just run `npm run start` to start the connection to back-end feed and the publisher.

The `RabbitWorker` starts with NodeJS UI's instance.

** Comments inside the code will try to guide you through the 'how' all this RabbitMQ Publisher program works. **
