import * as pino from 'pino';
import { cachedData } from './../index';
import { metricsLogger } from './../metrics_logger';
import { getAsianOdds} from './../data-handlers/outcomes/outcomes-filter';

let amqp = require('amqplib/callback_api');

// this variable can be modified if we ever want to have our RabbitMQ server hosted on a remote location.
const CONN_URL = 'amqp://127.0.0.1';

const logger: any = pino();
const rabbitPublisherLog = logger.child({type: 'rabbit_publisher_logger'});

// To get metrics for last received message of each channel
let messageLogger = {
  'oddsChannel_live': 0,
  'oddsChannel_prematch': 0,
  'gamecastChannel': 0,
  'pingChannel_live': 0,
  'pingChannel_prematch': 0
};

export class RabbitPublisher {

  // Which feed - live or prematch
  private which: any = null;

  // store the ampq connection instance
  private connection: any = null;

  // to clear - start the pingInterval (health checker)
  public pingInterval: any = null;

  // pingChannel is used to ping-pong between Publisher and Workers
  public pingChannel: any = null;
  public oddsChannel: any = null;
  public updatesChannel: any = null;
  public gamecastChannel: any = null;
  public recoveryInterval: any = null;

  // make the backend feed service instance available so we can 'inject' our channels and broadcast feeds data to broker
  public outcomesFeedService: any = null;

  constructor(outcomesFeedService: any, which: string) {
    this.outcomesFeedService = outcomesFeedService;
    this.which = which;
    this.startPublisher();
  }

  async startPublisher() {

    // If we are restarting on recovery process ; kill the previous interval
    if (this.pingInterval) {
      clearInterval(this.pingInterval);
    }

    try {

      this.connection = await this.startConnection();

      // The second argument of createChannel is the channel 'type'.
      // oddsChanel has to be a different kind of connection (RPC) : odds state is sent only by request from worker
      // as payload is to big (if we keep sending outcomes state over and over to the queue parsing work exhausts CPU & RES)

      this.oddsChannel = await this.createChannel(`oddsChannel_${this.which}`, 'rpc');

      // updates & gamecast data is sent to queue on each update we receive from feed
      // (this objects are not that big so no problem on sending to queue).

      this.updatesChannel = await this.createChannel(`updatesChannel_${this.which}`, 'publish');

      if (this.which === 'live') {
        this.gamecastChannel = await this.createChannel(`gamecastChannel`, 'publish');
      }

      // Monitor RabbitMQ Publisher healt
      this.pingChannel = await this.createChannel(`pingChannel_${this.which}`, 'publish');

      // We may not need pingChannel inside outcomesFeedService
      this.outcomesFeedService.channels = {
        [`oddsChannel_${this.which}`]: this.oddsChannel,
        [`updatesChannel_${this.which}`]: this.updatesChannel
      };

      if (this.which === 'live') {
        this.outcomesFeedService.channels['gamecastChannel'] = this.gamecastChannel;
      }

      // Channels needs to be closed if process exits. Otherwise they remain open

      // process.on('exit', (code) => {

      //   // Need to close channels if process exits..
      //   console.log('rabbit publisher exit. Closing channels...', code);

      //   [
      //     this.pingChannel,
      //     this.oddsChannel,
      //     this.updatesChannel,
      //     this.gamecastChannel
      //   ].forEach(channel => {

      //     try {
      //       channel.close();
      //     }
      //     catch (err) {
      //       console.log('channel not closed on exit', err);
      //     }

      //   });

      // });

      // Start health checker
      this.Ping();
    }

    catch(err) {
      console.log(err);
    }

  }

  startConnection() {

    let that = this;

    return new Promise((res, rej) => {

      // start connection to amqp server
      amqp.connect(CONN_URL + '?heartbeat=60', function (err, conn) {

        // retry connection in case of error
        if (err) {
          console.error('[AMQP]', err.message);
          return that.startRecovery();
        }

        console.log('Connection to RabbitMQ server established--', that.which);

        conn.on('error', function(err) {
          if (err.message !== 'Connection closing') {
            console.error('[AMQP] conn error', err.message);
          }
          console.log(err.message);
        });

        // Restore connection with amqp server if it drops
        conn.on('close', function() {
          console.error('[AMQP] reconnecting');
          return that.startRecovery();
        });

        res(conn);
      });

    });
  }

  startRecovery() {

    let that = this;
    let recoveryIntervalCounter = 5;

    if (this.recoveryInterval) return;

    process.removeAllListeners();

    this.recoveryInterval = setInterval(() => {

      recoveryIntervalCounter --;
      console.log(`Recoverying RabbitMQ connection in ${recoveryIntervalCounter} secs...`);
      if (recoveryIntervalCounter === 0) {
        that.startPublisher();
        clearInterval(that.recoveryInterval);
        that.recoveryInterval = null;
      }

    }, 1000);
}

// @exchange : channel's name
// @type: channel's type : 'rpc' (Remote Procedure Call) or 'publish' (Publish-Subscribe).
// More info on channel configurations: https://bit.ly/2pBxnQD

createChannel(exchange: string, type: string) {

  let that = this;

    return new Promise((res, rej) => {
      that.connection.createChannel(function(err, channel) {

        if (err) {
          return rej(err);
        }

        if (type === 'publish') {

          // Channels are not 'durable' - this means a new message will delete the previous one.
          // Otherwise the messages get's on a queue if not consumed - for our purposes there is no point in comsuming old messages.

          channel.assertExchange(exchange, 'fanout', {
            durable: false
          });
        }

        channel.assertQueue(exchange, {
          durable: false
        });

        // We will only parse/send the huge outcomes state by request. So channel config needs to be different ..

        if (type === 'rpc') {

          channel.consume(exchange, function reply(msg: any) {

            console.log('I will send outcomes state', that.which);

            let msgObj = that.which === 'live' ? {
              outcomes: cachedData.outcomes,
              flattenOutcomes: {
                allOddsByEvent: cachedData.flattenOutcomes.allOddsByEvent,
                mainOddsBySport: cachedData.flattenOutcomes.mainOddsBySport,
                asianOddsBySport: getAsianOdds(cachedData, that.which)
              }
            } : {
              outcomesPrematch: cachedData.outcomesPrematch,
              flattenOutcomes: {
                allPrematchOddsByEvent: cachedData.flattenOutcomes.allPrematchOddsByEvent,
                mainPrematchOddsBySport: cachedData.flattenOutcomes.mainPrematchOddsBySport
              }
            };

            channel.sendToQueue(msg.properties.replyTo,
              Buffer.from(JSON.stringify(msgObj)), {
              correlationId: msg.properties.correlationId
            });
            console.log('sending outcomes copy', that.which);
            channel.ack(msg);

          });

        }

        channel.on('close', function() {

          try {
            that.connection.close();
            console.log('[AMQP] channel closed', exchange);
          }

          catch (err) {

            console.log('connection already beeing closed');

          }

        });

        channel.on('error', function() {
          that.connection.close();
          console.log('[AMQP] channel closed', exchange);
        });

        res(channel);

      });

    });

  }

  Ping() {
    let that = this;

    this.pingInterval = setInterval(() => {
      let currentTime = new Date().getTime();
      PublishToRabbit(that.pingChannel, `pingChannel_${that.which}`, { 'ts': currentTime });
    }, 5000);
  }

}

// Publish messages to brokers
// @ channel_: channel instance;
// @ exchange: channel's name
// @ msg : msg to broadcast

export function PublishToRabbit(channel_: any, exchange: any, msg: any) {

  messageLogger[exchange] = new Date().getTime();

  try {
    channel_.publish(exchange, '', Buffer.from(JSON.stringify(msg)),
    {
      // We don't need messages to persist if they are not consumed
      persistent: false
    },
    function(err: any, ok: any) {
      if (err) {
        console.error(`[AMQP] publish error ${exchange}`, err);
      }
      console.log('Msg Sent %s', exchange);
    });

  }

  catch(err) {
    console.log('channel is closed');
  }

};

// Get metrics on each channel's last emmited message time stamp.
function LastMessageLogger() {

  setInterval(() => {
    Object.keys(messageLogger)
      .forEach(logger => {
        rabbitPublisherLog.warn(`${logger} last received message ${messageLogger[logger]}`);
      });
      metricsLogger();
  }, 15000);

}

LastMessageLogger();
