import { cachedData } from './index';
import OutcomesDataHandlerServer from './data-handlers/outcomes/server';
import { IUpdateOutcome, IDeleteOutcome } from './data-handlers/outcomes/interfaces';

const logger = require('pino')({ enabled: !!process.env.NOLOG } );
const outcomesLog = logger.child({type: 'outcomes'});

const now = () => +new Date();

let timeOut: any;
let lastUpdate = now();
const INTERVAL_BETWEEN_UPDATES = 1500;
let evs: { [eventId: string]: any } = {};

export const usedStatuses = ['Open', 'TemporarilySuspended', 'AdministrativelySuspendedLTD'];

export function updateOutcomes(currentOutcomes: any, incomingOdds: any, channels: any) {

	const killedNonLstOutcomes: any = [];
	let haveWrittenLog = false;

  incomingOdds.forEach(ev => {

		const eventId = ev.event;
		const bookmakerId = ev.bookmakerId;
		const outcomeId = ev.outcomeId;
		const isLST = ev.main;
		const isAsian = ev.asian;
		const id = ev.id;
		const sport = ev.sport;
		const lastUpdate = ev.lastupdate;
		const status = ev.status_;
		const status_ = ev.status_;
		const display = ev.display;
		const oddIsLive = ev.oddIsLive;
		const american = ev.american;
		const price = ev.price;
		const marketPrice = ev.marketPrice;
		const bet = ev.bet;
		const line = ev.line;
		const suspended = ev.suspended;

		let oddObj: any = ev;

		if (!currentOutcomes[eventId]) {
			currentOutcomes[eventId] = {};
		}

		const eventOutcomes = currentOutcomes[eventId];

		if (eventOutcomes && !eventOutcomes[bookmakerId]) {
			eventOutcomes[bookmakerId] = {};
		}

		if (eventOutcomes && eventOutcomes[bookmakerId] && !eventOutcomes[bookmakerId][outcomeId]) {
			eventOutcomes[bookmakerId][outcomeId] = {};

		}

    const outcome = eventOutcomes[bookmakerId][outcomeId];

    let shouldUpdate = false;

		if (!haveWrittenLog) {
			haveWrittenLog = true;
			const feedLastUpdate = lastUpdate;
			const feedDate = new Date(feedLastUpdate);
			const currentDateTime = new Date().toUTCString();

			// dataTracker['lastSuccesfulOddsUpdateDelta'] = Date.now() - feedLastUpdate;
			// dataTracker['timeOfLastSuccessfulOddsUpdateFromFeed'] = feedLastUpdate;

		}

		let updateObj: any = {};

		if (usedStatuses.indexOf(status) !== -1) {
			const odd = outcome[id];

			if (!odd || (odd.price !== ev.price || odd.american !== ev.american || odd.suspended !== ev.suspended || odd.status !== status)) {

				// oddsMonitorBuilder(odd, id, ev.status, ev.oddIsLive, ev.display, eventId, outcomeId);

				outcome[id] = {
					order: ev.order,
					column: ev.column,
					price: ev.price,
					american: ev.american,
					display: ev.display,
					suspended: ev.suspended,
					replacers: ev.replacers,
					marketPrice: ev.marketPrice,
					line: ev.line,
					bet: ev.bet,
					asian: isAsian,
					main: isLST,
					status_: status,
					status: status !== 'AdministrativelySuspendedLTD' ? undefined : status,
					oddIsLive: ev.oddIsLive,
					bookmakerId,
					outcome: outcomeId,
					id,
					event: eventId,
					sport: ev.sport,
					lastupdate: ev.lastupdate
				};
				shouldUpdate = true;
			}

			const oddFlatten = oddIsLive === 1 ? cachedData.flattenOutcomes.allOddsByEvent[eventId] && cachedData.flattenOutcomes.allOddsByEvent[eventId][id] : cachedData.flattenOutcomes.allPrematchOddsByEvent[eventId] && cachedData.flattenOutcomes.allPrematchOddsByEvent[eventId][id];

			if (!oddFlatten || (oddFlatten.price !== ev.price || oddFlatten.american !== ev.american || oddFlatten.suspended !== ev.suspended || oddFlatten.status !== status)) {

				if (!oddFlatten) {

					cachedData.flattenOutcomes = OutcomesDataHandlerServer
						.createOutcome(cachedData.flattenOutcomes, oddObj);

				}
				// console.log(oddFlatten);
				if (oddFlatten) {

					[
						{ condition: oddFlatten.line !== line, key: 'line', oldKey: 'oldLine', value: line, oldValue: oddFlatten.line },
						{ condition: oddFlatten.price !== price, key: 'price', oldKey: 'oldPrice', value: price, oldValue: oddFlatten.price },
						{ condition: oddFlatten.status_ !== status, key: 'status', oldKey: 'oldStatus', value: status, oldValue: oddFlatten.status_ },
						{ condition: oddFlatten.display !== display, key: 'display', oldKey: 'oldDisplay', value: display, oldValue: oddFlatten.display },
						{ condition: oddFlatten.american !== american, key: 'american', oldKey: 'oldAmerican', value: american, oldValue: oddFlatten.american },
						{ condition: oddFlatten.suspended !== suspended, key: 'suspended', oldKey: 'oldSuspended', value: suspended, oldValue: oddFlatten.suspended },
						{ condition: oddFlatten.marketPrice !== marketPrice, key: 'marketPrice', oldKey: 'oldmarketPrice', value: marketPrice, oldValue: oddFlatten.marketPrice }

					].forEach(item => {

						if (item.condition) {

							updateObj = {
								...updateObj,

								[id]: {
									...updateObj[id],
									[item.key]: item.value,
									eventId,
									ts: Date.now()

								}
							};

						}

					});

					let updateOutcomePayload: IUpdateOutcome = {
						updateObj: updateObj[id],
						oddIsLive,
						oddId: id,
						event: eventId,
						asian: isAsian,
						main: isLST,
						sport

					};

					cachedData.flattenOutcomes = OutcomesDataHandlerServer
						.updateOutcome(cachedData.flattenOutcomes, updateOutcomePayload);

				}
				// console.log(JSON.stringify(cachedData.flattenOutcomes));
			}

		}
		else { // this means status is suspended or something else. So we need to delete the odd from UI's outcome state
			if (outcome[id]) {
				shouldUpdate = true;

				// console.log(status_, 'status_ before delete');

				let deleteOutcomePayload: IDeleteOutcome = {
					oddId: id,
					oddIsLive,
					asian: isAsian,
					event: eventId,
					main: isLST,
					sport

				};

				cachedData.flattenOutcomes = OutcomesDataHandlerServer
					.deleteOutcome(cachedData.flattenOutcomes, deleteOutcomePayload);

				if (eventOutcomes[bookmakerId] && eventOutcomes[bookmakerId][outcomeId] && eventOutcomes[bookmakerId][outcomeId].hasOwnProperty(id)) {
					killedNonLstOutcomes.push([id, outcomeId, eventId, bet, line, status]);
					const odd = eventOutcomes[bookmakerId][outcomeId][id];
					// oddsMonitorBuilder(odd, id, ev.status, ev.oddIsLive, ev.display, eventId, outcomeId);
					delete eventOutcomes[bookmakerId][outcomeId][id];

				}

			}
		}

		if (shouldUpdate) {
			evs[eventId] = eventOutcomes;

		}

  });

  const t = now() - lastUpdate;

  if (t >= INTERVAL_BETWEEN_UPDATES) {

		lastUpdate = now();

		evs = null;
		evs = {};

  }
  else {
		clearTimeout(timeOut);
		timeOut = setTimeout(() => updateOutcomes(currentOutcomes, [], channels), INTERVAL_BETWEEN_UPDATES);
	}

	// Write log entry about the deleted odds.
	const genDeletedOddsLogString = function(singleEntry: any, lstFlag: any) {
		const _id = singleEntry[0];
		const _outcomeId = singleEntry[1];
		const _eventId = singleEntry[2];
		const _bet = singleEntry[3];
		const _line = singleEntry[4];
		const _status = singleEntry[5];
		const dt = new Date();

		const generatedString = `${dt.toUTCString()} [ts] ${dt.getTime()} [id] ${_id} [outcomeId] ${_outcomeId} [eventId] ${_eventId} [bet] ${_bet} [line] ${_line} [status] ${_status} was deleted via ws/outcomes.ts updateOutcomes - ${lstFlag}`;
		// console.log(generatedString);
		return generatedString;
	};

	killedNonLstOutcomes.forEach((item: any) => {
		outcomesLog.debug(genDeletedOddsLogString(item, 'is NOT LST'));
	});
}