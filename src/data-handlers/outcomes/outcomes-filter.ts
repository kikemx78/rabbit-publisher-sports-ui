export let getAsianOdds = (cachedData, which) => {

  let oddIsLive_ = which === 'live' ? 1 : 0;
  let asianOddsLive = {};

  Object.keys(cachedData.flattenOutcomes.asianOddsBySport)
  .forEach(sport => {
    Object.keys(cachedData.flattenOutcomes.asianOddsBySport[sport])
      .forEach(otcId => {
        if (cachedData.flattenOutcomes.asianOddsBySport[sport][otcId].oddIsLive === oddIsLive_) {
          if (!asianOddsLive[sport]) {
            asianOddsLive[sport] = {};
          }
          asianOddsLive[sport][otcId] = cachedData.flattenOutcomes.asianOddsBySport[sport][otcId];
        }
      });
  });

  return asianOddsLive;

};