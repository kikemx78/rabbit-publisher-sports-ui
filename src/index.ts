import * as pino from 'pino';
import config from './config';
import * as WebSocket from 'ws';

// Organize feeds data on our cachedData structure and handles updates
import { updateOutcomes } from './updateOutcomes';

// PublishToRabbit sends a message to broker so RabbitWorkers can consume it.
import { RabbitPublisher, PublishToRabbit } from './RabbitPublisher';

// Parse feed's raw data on to our outcomes structure
import { BackendOddsMessageParser } from './backend_odds_message_parser';

const logger: any = pino();
const feedWebsocketLog = logger.child({type: 'web_socket_start'});

// The object in which we will maintain the outcomes state
export const cachedData: {
  flattenOutcomes: any,
  outcomesPrematch: any,
  outcomes: any
} = {
  flattenOutcomes: {
    allPrematchOddsByEvent: {},
    mainPrematchOddsBySport: {},
    asianOddsBySport: {},
    allOddsByEvent: {},
    mainOddsBySport: {}
  },
  outcomesPrematch: {},
  outcomes: {}
};

// Configure this under the .env file - check instructions on env.sample

export const HAVE_LIVE_ACCESS = config.liveOddsWebsocketUrl.length > 0 && config.liveOddsUsername.length > 0 && config.liveOddsPassword.length > 0;
export const HAVE_PREMATCH_ACCESS = config.prematchOddsWebsocketUrl.length > 0 && config.prematchOddsUsername.length > 0 && config.prematchOddsPassword.length > 0;

export default class OutcomesFeedService {

    // wheter connection with backend feed needs to be restarted due to a connectiviy problem
    private timeOut: any = null;
    // store - handle ping-pong's logic interval 
    private intervalPing: any = null;

    // live or prematch
    private which: any;

    // store backend feed's instance
    private backendClient: any;

    // check connectivityStatus with backend feed
    private connectivityStatus: boolean = false;
    private haveDataFetchIntervalsRunning: boolean = false;

    // Register closed events
    private closedFeedEventCounter: number = 0;

    // check connectivity status with back end feed
    private lastServerConnectionMsgToConnectedUsers: number = 0;
    // last message from feed ts - to determine connectivity status
    private lastFeedMessage: Date = new Date(0);
    private foundDisconnectEvent: boolean = false;

    // 'channels' store RabbitPublisher's channels inside this class. We need them here to send data to the broker.
    // Available channels :
    // 'outcomesChannel' (live and prematch) - handles outcomes state;
    // 'updatesChannel' (live and prematch) - handles outcomes updates;
    // 'gamecastChannel' (live) - handles gamecast data.

    private channels: any;

    constructor(which: string) {

      this.which = which;
      let that: any = this;

      if (!this.haveDataFetchIntervalsRunning) {

        this.timeOut = setInterval(function() {

          const now = new Date().getTime();

          if (that.foundDisconnectEvent) {
            that.foundDisconnectEvent = false;
            feedWebsocketLog.warn('restarting connection after close event');

            feedWebsocketLog.info(`Calling start() to re-start connection to ${that.which} ws`);
            that.start();

            // We just called that.start(), meaning we are going to try to start a WS connection.
            // We return now to leave this function so that we don't ALSO slip into the 'Check connectivity status' below.
            return;
          }

          // Check connecitvity status
          if (now - that.lastFeedMessage.getTime() > 60000) {
            that.connectivityStatus = false;

            feedWebsocketLog.error(`${new Date().toString()} Flagging ${that.which} connectivity status as a problem - last activity was ${now - that.lastFeedMessage.getTime()} ms ago` );
            that.closedConnection(504, `${that.which} - Last activity was ${now - that.lastFeedMessage.getTime()} ms ago`, that.which);
          }

        }, 10000);

        this.haveDataFetchIntervalsRunning = true;
      }

      feedWebsocketLog.info(`instantiating ${which} socket client`);
      console.log(this.closedFeedEventCounter, 'closedEve');
      this.start();

    }

    start() {

      clearInterval(this.intervalPing);

      let client = this.backendClient;

      // If we are recoverying, we need to kill the old client connection
  
      if (client) {
        feedWebsocketLog.info(`start() - Closing connection on ${this.which} before starting the new connection`);
        client.terminate();
        client = null;
      }

      let feedURL: any = {
        'live': config.liveOddsWebsocketUrl,
        'prematch': config.prematchOddsWebsocketUrl
      };

      const ip = feedURL[this.which];
      client = new WebSocket(ip);
      this.backendClient = client;
      this.connectivityStatus = true;
      this.lastFeedMessage = new Date();

      let that: any = this;

      this.intervalPing = setInterval(() => {
        that.ping(that);
      }, 10000);

      feedWebsocketLog.info(`${new Date().toString()} Starting WS connection to ${ip}; for ${this.which}`);

      client.on('open', function() {

        feedWebsocketLog.info(`${new Date().toString()} connection to ${that.which} feed established - sending login`);
        that.lastFeedMessage = new Date();

        // we need to authenticate to start receiving messages from feed
        that.doFeedAuthentication();

        if (that.which === 'live') {

          // Uncomment to replicate Aprubte Disconnection event at an interval
          // that.testInterval(client);
        }


      });

      client.on('message', function(msg: any) {
        that.handleMessage(msg, that.which);
      });

      client.on('error', function(err: any) {
        feedWebsocketLog.error(`Caught WebSocket error for ${that.which}: ${err}`);
        that.closedConnection(505, `error received on client ${that.which}: ${err}`);
      });

    }

    doFeedAuthentication() {

      if (this.backendClient && this.backendClient.readyState === this.backendClient.OPEN) {

        let credentials: any = {
          'live': {
            username: config.liveOddsUsername,
            password: config.liveOddsPassword
          },
          'prematch': {
            username: config.prematchOddsUsername,
            password: config.prematchOddsPassword
          }
        };

        const loginMessage = {
          requestType: 'login',
          data: {
            username: credentials[this.which].username,
            password: credentials[this.which].password
          }
        };
        this.backendClient.send(JSON.stringify(loginMessage));

      }
      else {
        feedWebsocketLog.error('ERROR in doFeedAuthentication: specified client is not connected or readyState is not OPEN');
      }

    }

    ping(that: any) {
      that.sendPingMessage();
    }

    sendPingMessage() {
      if (this.backendClient.readyState !== this.backendClient.OPEN) {
        feedWebsocketLog.error(`error from ping ${this.backendClient.readyState}`);
        this.closedConnection(0, `Connection close called by ping() for ${this.which}`);
      } else {
        feedWebsocketLog.error(`backendClient ping ${this.which}`);
        this.backendClient.send(JSON.stringify({
          requestType: 'ping',
          data: {}
        }));
      }
    }

    closedConnection(code: any, reason: any) {

      feedWebsocketLog.warn(`Close detected.  Which:  ${this.which}, [code] ${code}, [reason] ${reason}`);
      this.foundDisconnectEvent = true;

      feedWebsocketLog.warn(`Clearing cached data from ${this.which} socket`);

      // Empty cached data object to avoid duplicate outcomes when feed's connection is restored.

      if (this.which === 'live') {

        cachedData.outcomes = {};
        cachedData.flattenOutcomes.asianOddsBySport = {};
        cachedData.flattenOutcomes.mainOddsBySport = {};
        cachedData.flattenOutcomes.allOddsByEvent = {};

      } else {

        cachedData.outcomesPrematch = {};
        cachedData.flattenOutcomes.allPrematchOddsByEvent = {};
        cachedData.flattenOutcomes.mainPrematchOddsBySport = {};

      }

      this.closedFeedEventCounter ++;
      feedWebsocketLog.error(`Abrupte disconnection ${this.which} No. ${this.closedFeedEventCounter}`);

      // RabbitWorkers need to be notified of closed connection. We use the updatesChannel for this ...
      if (this.channels && this.channels[`updatesChannel_${this.which}`]) {

        let msg = {
          'closedConnection': this.which
        };

        PublishToRabbit(this.channels[`updatesChannel_${this.which}`], `updatesChannel_${this.which}`, msg);
      }

      // Close RabbitPublishers channels - RabbitPublisher restarts once a channel gets disconnected

      if (this.channels) {

        Object.keys(this.channels)
          .forEach((channel: any) => {
            try {
              this.channels[channel].close();
            }
            catch(err) {
              console.log('channel is already closed');
            }
          });
      }

    }

    handleMessage(msg: any) {

      this.lastFeedMessage = new Date();
      this.connectivityStatus = true;

      const received = JSON.parse(msg);

      switch (received.requestType) {
        case 'odds':

          const oddsData = received.data || {};
          const parsedOdds = BackendOddsMessageParser.ParseData(oddsData);

          // Send updates to updatesChannel. New data is a smaller object and is sent to queue each time we get an update from feed, once a new update message arrives to the queue
          // the old message gets deleted.
          if (this.channels && this.channels[`updatesChannel_${this.which}`]) {
            PublishToRabbit(this.channels[`updatesChannel_${this.which}`], `updatesChannel_${this.which}`, parsedOdds);
          }

          // outcomes state is built under updateOutcomes. Outcomes state is sent to
          // queue only by request using the outomesChannel (otherwise parsing work exhausts CPU).
          if (this.which === 'live') {
            updateOutcomes(cachedData.outcomes, parsedOdds, this.channels);
          } else {
            updateOutcomes(cachedData.outcomesPrematch, parsedOdds, this.channels);
          }
          break;
        case 'gamecast':
        case 'gamecastpush':
          if (!received.data) break;
          if (this.which === 'prematch') {
            console.log('receiving gamecast data on prematch...');
          }

          // Send gamecast data to broker.
          if (this.channels && this.channels[`gamecastChannel`]) {
            PublishToRabbit(this.channels[`gamecastChannel`], `gamecastChannel`, received.data);
          }
          break;
        case 'events':
          break;
        case 'pong':
          feedWebsocketLog.info(`pong for ${this.which}`);
          // Pong messages from server are in reply to our ping; we don't need to do anything with them
          break;
        default:
          console.log(`Unknown message: `, msg);
          break;
      }

      let currentTime = new Date().getTime();

      if ( (currentTime - this.lastServerConnectionMsgToConnectedUsers) > 10000) {
        this.sendBackendConnectivityStatusToConnectedUsers();
        this.lastServerConnectionMsgToConnectedUsers = currentTime;
      }

      if (received.command === 'timeout') {
        this.start();
      }

    }

    sendBackendConnectivityStatusToConnectedUsers() {
      let isClosedConnection = this.haveBackendConnectivityProblem();
        // feedWebsocketLog.info(`Telling clients that ${this.which} has disconnected`);
    }

    haveBackendConnectivityProblem() {
      let feedOK = true;
      if (!this.connectivityStatus) {
        feedOK = false;
      }

      feedWebsocketLog.info(`HaveBackendConnectivityProblem is being evaluated. [${this.which}Ok] ${this.which}; [feedOK] ${feedOK}`);

      if (!feedOK) return true;
      return false;
    }

    // This function is used to create an Abrupt disconnection event (for testing the recovery logic of this service)

    testInterval(client: any) {
      let intervalCounter = 0;
      let that = this;
      let testInterval_ = setInterval(() => {
        intervalCounter ++;
        console.log(intervalCounter);
        if (intervalCounter >= 60) {
          if (client.readyState === client.OPEN) {
            that.closedConnection(0, `close hack ${this.which}`);
            clearInterval(testInterval_);
          }
        }
      }, 1000);

    }

}

if (HAVE_LIVE_ACCESS) {
  let outcomesFeedServiceLive = new OutcomesFeedService('live');

  // we need to pass the OutcomesFeedService to our RabbitPublisher instance to 'inject' the publisher's channels
  // and broadcast feed updates.

  new RabbitPublisher(outcomesFeedServiceLive, 'live');
}

if (HAVE_PREMATCH_ACCESS) {
  let outcomesFeedServicePrematch = new OutcomesFeedService('prematch');
  new RabbitPublisher(outcomesFeedServicePrematch, 'prematch');
}
