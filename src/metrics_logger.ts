import { cachedData } from './index';

let liveOdds = 0;
let cachedOdds = 0;
let prematchOdds = 0;
let lastLiveOddsReading = 0;
let lastPrematchOddsReading = 0;
let lastCachedOutcomesReading = 0;

const logger = require('pino')();
const generalMetricsLog = logger.child({ type: 'general_metrics' });

export const metricsLogger = () => {

  generalMetricsLog.info('Last Total cached outcomes reading', lastCachedOutcomesReading);
  generalMetricsLog.info('Last Total Live outcomes reading', lastLiveOddsReading);
  generalMetricsLog.info('Last Total Prematch outcomes reading', lastPrematchOddsReading);

  if (lastCachedOutcomesReading < cachedOdds) lastCachedOutcomesReading = cachedOdds;
  if (lastPrematchOddsReading < prematchOdds) lastPrematchOddsReading = prematchOdds;
  if (lastLiveOddsReading < liveOdds) lastLiveOddsReading = liveOdds;

  cachedOdds = 0;
  prematchOdds = 0;
  liveOdds = 0;

  Object.keys(cachedData.outcomes)
  .forEach(eventId => {
    Object.keys(cachedData.outcomes[eventId])
      .forEach((otcId: any) => {
        cachedOdds ++;
      });
  });

  Object.keys(cachedData.flattenOutcomes.allPrematchOddsByEvent)
    .forEach(eventId => {
      Object.keys(cachedData.flattenOutcomes.allPrematchOddsByEvent[eventId])
        .forEach((otcId: any) => {
          prematchOdds ++;
        });
    });

  Object.keys(cachedData.flattenOutcomes.allOddsByEvent)
  .forEach(eventId => {
    Object.keys(cachedData.flattenOutcomes.allOddsByEvent[eventId])
      .forEach((otcId: any) => {
        liveOdds ++;
      });
  });

  generalMetricsLog.info('Total Live outcomes ', liveOdds);
  generalMetricsLog.info('Total Prematch outcomes ', prematchOdds);

};
