import { cachedData } from './index';

// This function is used to store the date we got from publisher inside
// our cachedData object. It will run after worker gets initial outcomes data copy

export function rabbitOutcomesState(outcomesState: any, which: string) {
  // We store the rabbit outcomesState object inside our old cachedData object

  if (which === 'live') {
    try {

      outcomesState = JSON.parse(outcomesState.toString());
      console.log('getting asian');

      cachedData.outcomes = outcomesState.outcomes;

    cachedData.flattenOutcomes = {
      ...cachedData.flattenOutcomes,
      allOddsByEvent: outcomesState.flattenOutcomes.allOddsByEvent,
      mainOddsBySport: outcomesState.flattenOutcomes.mainOddsBySport,
      asianOddsBySport: outcomesState.flattenOutcomes.asianOddsBySport
    };

    }
    catch {
      console.log('RabbitMQ outcomes state message string has invalid JSON format', which);
    }
  }

  if (which === 'prematch') {

    try {

      outcomesState = JSON.parse(outcomesState.toString());
      console.log(outcomesState);

      cachedData.outcomesPrematch = outcomesState.outcomesPrematch;
      cachedData.flattenOutcomes = {
        ...cachedData.flattenOutcomes,
        allPrematchOddsByEvent: outcomesState.flattenOutcomes.allPrematchOddsByEvent,
        mainPrematchOddsBySport: outcomesState.flattenOutcomes.mainPrematchOddsBySport
      };

    }
    catch {
      console.log('RabbitMQ outcomes state message string has invalid JSON format', which);
    }
  }

}