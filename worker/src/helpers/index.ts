export const cachedData: any = {
  outcomes: {},
  outcomesPrematch: {},
  flattenOutcomes: {
    allOddsByEvent: {},
    mainOddsBySport: {},
    asianOddsBySport: {},
    mainPrematchOddsBySport: {},
    allPrematchOddsByEvent: {}
  }
};
