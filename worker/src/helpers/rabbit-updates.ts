// This function runs every time we get an update from our backend feed. It executes updateOutcomes method.
// This last method does three things :
// 1. updates the workers outcomes state with new data from our feed.
// 2. Builds the update object that will be delivered to clients (browsers) so they update their own outcomes state
// 3. Calculate number of open markets after each update

import { cachedData } from './index';
import { updateOutcomes } from './../updateOutcomes';

let lastMarketCountUpdateLive = 0;
let lastMarketCountUpdatePrematch = 0;

export function rabbitUpdates(updateObject: any, which: string) {

  let parsedOdds = null;
  try {
    parsedOdds = JSON.parse(updateObject.toString());
  }
  catch {
    console.log('RabbitMQ updates message string has invalid JSON format');
  }

  if (parsedOdds) {

    updateOutcomes(cachedData.outcomes, parsedOdds);

    let currentTime = new Date().getTime();
    let lastMarketCountUpdate = which === 'live' ? lastMarketCountUpdateLive : lastMarketCountUpdatePrematch;

    if ( (currentTime - lastMarketCountUpdate) > 5000) {

      let isLive = which === 'live' ? 1 : 0;
      let evs = which === 'live' ? cachedData.flattenEvents : cachedData.prematch;

      // checkMarketNumber(server, evs, cachedData.flattenOutcomes, cachedData.outcomeList, isLive);
      lastMarketCountUpdate = currentTime;

      // let type_ = which === 'live' ? CLOSED_CONNECTION_LIVE : CLOSED_CONNECTION_PREMATCH;

      // let messageParams = {
      //   type: type_,
      //   payload: false
      // };

      // broadcastToAllSockets(server, messageParams);
      // Notify browsers connection is open
    }
  }

}