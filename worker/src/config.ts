require('dotenv').config();

function buildConfig() {

  let environment = process.env;

  let configObj: any = {
    liveOddsWebsocketUrl: environment['LIVE_ODDS_WEBSOCKET_URL'] || '',
    liveOddsUsername: environment['LIVE_ODDS_USERNAME'] || '',
    liveOddsPassword: environment['LIVE_ODDS_PASSWORD'] || '',
    prematchOddsWebsocketUrl: environment['PREMATCH_ODDS_WEBSOCKET_URL'] || '',
    prematchOddsUsername: environment['PREMATCH_ODDS_USERNAME'] || '',
    prematchOddsPassword: environment['PREMATCH_ODDS_PASSWORD'] || '',
    mockBookmaker: environment['MOCK_BOOKMAKER'] || 103
  };

  return configObj;
};

let config = buildConfig();

export default config;
