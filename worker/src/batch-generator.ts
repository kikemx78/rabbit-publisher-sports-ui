// **
//
// @type 'main', 'regular', 'asian', 'mainPrematch'
// @ref sportId, eventId
// @action 'ADD', 'UPDATE', 'DELETE'
// **

export const batchGenerator = (updateObj: any, updateBatch: any, oddObj: any, id: any, type: any, ref: any, action: any) => {

    if (!updateBatch[type]) {
      updateBatch[type] = {};
    }
  
    if (!updateBatch[type][ref]) {
      updateBatch[type][ref] = {};
    }
  
    if (!updateBatch[type][ref][action]) {
      updateBatch[type][ref][action] = {};
    }
  
    // // If an UPDATE arrives when there is also a DELETE - remove DELETE
    if (updateBatch[type][ref]['DELETE'] && updateBatch[type][ref]['DELETE'][id] && action === 'UPDATE') {
      delete updateBatch[type][ref]['DELETE'][id];
    }
  
    // // If an UPDATE arrives when there is also an ADD - mutate it with new values and set action to 'ADD'
    if (updateBatch[type][ref]['ADD'] && updateBatch[type][ref]['ADD'].length > 0 && action === 'UPDATE') {
  
      updateObj = {
        ...updateObj,
          [id]: oddObj
      };
  
      delete updateBatch[type][ref]['ADD'][id];
      action = 'ADD';
    }
  
    // // If an ADD arrives when there is also a DELETE - remove DELETE
    if (updateBatch[type][ref]['DELETE'] && updateBatch[type][ref]['DELETE'].length > 0 && action === 'ADD') {
      delete updateBatch[type][ref]['DELETE'][id];
    }
  
    updateBatch[type][ref][action][id] = updateObj[id];
  
    // If the same odd is present on ADD and DELETE on the same batch...
    if (updateBatch[type][ref]['ADD'] && updateBatch[type][ref]['DELETE']) {
  
      if (updateBatch[type][ref]['ADD'][id] && updateBatch[type][ref]['DELETE'][id]) {
  
        // And DELETE ts is older, we remove both items from batch as there is no need to add
        // an outcome that will be deleted in a few miliseconds...
  
        if (updateBatch[type][ref]['DELETE'][id].ts > updateBatch[type][ref]['ADD'][id].ts) {
          // console.log('fix was applied...');
          delete updateBatch[type][ref]['ADD'][id];
          // delete updateBatch[type][ref]['DELETE'][id];
  
        }
  
      }
    }
  
    return updateBatch;
  
  };
  