// This worker is only used for development - testing .

import config from './config';
import { metricsLogger } from './metrics_logger';
import { rabbitOutcomesState } from './helpers/rabbit-outcome-state';
import { rabbitUpdates } from './helpers/rabbit-updates';

export const logger = require('pino')();
const rabbitWorkerLog = logger.child({type: 'rabit_worker'});

let amqp = require('amqplib/callback_api');
const CONN_URL = 'amqp://127.0.0.1';

let messageLogger = {
  'oddsChannel_live': 0,
  'oddsChannel_prematch': 0,
  'gamecastChannel': 0,
  'pingChannel_live': 0,
  'pingChannel_prematch': 0
};

export default class RabbitWorker {

  public which: any = null;
  public channels: any = [];
  public emitter: any = null;
  public connection: any = null;
  public oddsChannel: any = null;
  public pingChannel: any = null;
  public updatesChannel: any = null;
  public gamecastChannel: any = null;
  public hasGotMessages: any = false;
  public heartbeatInterval: any = null;
  public correlationId: string = 'odds';
  public lastPingMessage = new Date().getTime();

  constructor(emitter: any, which: string) {
    this.emitter = emitter;
    this.which = which;
    this.startWorker();
  }

  async startWorker() {

    let that = this;
    this.channels = [];
    this.connection = null;
    this.heartbeatInterval = null;
    this.lastPingMessage = new Date().getTime();

    try {

      // Create connection with amqp server
      this.connection = await this.startConnection();
      rabbitWorkerLog.info('consumer connection established');

      // Create channels
      this.oddsChannel = await this.createChannel(`oddsChannel_${this.which}`, 'rpc');

      this.oddsChannel.channel.consume(this.oddsChannel.q.queue, function(msg: any) {

        if (msg.properties.correlationId === that.correlationId) {
          messageLogger[`oddsChannel_${that.which}`] = new Date().getTime();
          // rabbitWorkerLog.info('[x] Received %s', `oddsChannel_${that.which}`);
          that.emitter.emit(`outcomesStateReceived_${that.which}`, msg.content);
          console.time('label1');
          setTimeout(() => {
            // We close channel once we have received the outcomes state
            that.oddsChannel.channel.close();
          }, 500);
        }

      }, {
        noAck: true
      });

      // Odds channel works on a per requet logic (it requests the data and channel is closed afterwards)

      this.oddsChannel.channel.sendToQueue(`oddsChannel_${this.which}`,
        Buffer.from('odds'), {
          correlationId: that.correlationId,
          replyTo: this.oddsChannel.q.queue
      });

      this.channels = [...this.channels, this.oddsChannel.channel];

      this.updatesChannel = await this.createChannel(`updatesChannel_${this.which}`, 'publish');

      this.updatesChannel.channel.consume(this.updatesChannel.q.queue, function(msg: any) {
        if (msg.content) {

          // This means we got a disconnect event from fedd.
          // We need to restart our worker...

          if (msg.content.toString().includes('closedConnection')) {

            that.connection.close();

            return;
          }

          that.emitter.emit(`updatesReceived_${that.which}`, msg.content);
          messageLogger[`updatesChannel_${that.which}`] = new Date().getTime();
          // rabbitWorkerLog.info('[x] Received %s', `updatesChannel_${that.which}`);
        }
      }, {
        noAck: true
      });

      this.channels = [...this.channels, this.updatesChannel.channel];

      if (this.which === 'live') {

        this.gamecastChannel = await this.createChannel('gamecastChannel', 'publish');

        this.gamecastChannel.channel.consume(this.gamecastChannel.q.queue, function(msg: any) {
          if (msg.content) {

            that.emitter.emit('gamecastReceived', msg.content);
            messageLogger[`gamecastChannel`] = new Date().getTime();
            // rabbitWorkerLog.info('[x] Received %s', 'gamecastChannel');
          }
        }, {
          noAck: true
        });

        this.channels = [...this.channels, this.gamecastChannel.channel];

      }

      this.pingChannel = await this.createChannel(`pingChannel_${this.which}`, 'publish');
      this.pingChannel.channel.consume(this.pingChannel.q.queue, function(msg: any) {

        if (msg.content) {

          let ping = JSON.parse(msg.content.toString());
          that.lastPingMessage = ping.ts;
          messageLogger[`pingChannel_${that.which}`] = new Date().getTime();
          // rabbitWorkerLog.info('[x] Received %s', `pingChannel_${that.which}`);
        }

      }, {
        noAck: true
      });

      this.channels = [...this.channels, this.pingChannel.channel];

      that.healthChecker();

      process.on('exit', (code) => {
        // Need to close channels if process exits..
        console.log('rabbit worker exit. Closing channels...', code);

        // this.channels
        //   .forEach((channel: any) => {
        //     try {
        //       channel.close();
        //     }
        //     catch (err) {
        //       console.log('channel not closed on exit', err);
        //     }
        //   });
      });
    }

    catch (err) {
      console.log(err);
    }
  }

  startConnection() {

    let that = this;

    return new Promise((res, rej) => {

      amqp.connect(CONN_URL + '?heartbeat=60', function(error0: any, connection: any) {

        if (error0) {
          let { code, address } = error0;
          return setTimeout(() => that.startRecovery(), 1500);
        }

        connection.on('error', function(err: any) {
          console.log(err);
          // rabbitWorkerLog.error('[AMQP] conn error', err.message);
        });

        connection.on('close', function() {
          rabbitWorkerLog.error('[AMQP] reconnecting');
          return setTimeout(() => that.startRecovery(), 1500);
        });

        res(connection);

      });

    });

  }

  startRecovery() {
    rabbitWorkerLog.info('start recovery process...', this.which);

    // Notify instance we are closing connection...

    let msg = {
      'closedConnection': this.which
    };

    this.emitter.emit('closedConnection', JSON.stringify(msg));

    if (this.heartbeatInterval) {
      clearInterval(this.heartbeatInterval);
    }

    let that = this;

    let recoveryIntervalCounter = 5;
    let recoveryInterval = setInterval(() => {
      recoveryIntervalCounter --;
      rabbitWorkerLog.info(`Worker recoverying RabbitMQ connection in ${recoveryIntervalCounter} secs...`);
      if (recoveryIntervalCounter === 0) {
        that.startWorker();
        clearInterval(recoveryInterval);
      }
    }, 1000);

  }

  createChannel(exchange: any, type: string) {

    let connection = this.connection;

    return new Promise((res, rej) => {
      connection.createChannel(function(error1: any, channel: any) {
        if (error1) {
          return rej(error1);
        }

        if (type === 'publish') {
          channel.assertExchange(exchange, 'fanout', {
            durable: false
          });
        }

        channel.on('close', function() {
          rabbitWorkerLog.error('[AMQP] channel closed', exchange);
        });

        channel.on('error', function(err: any) {
          console.log(err);
          /// rabbitWorkerLog.error('[AMQP] conn error', err.message);
        });

        // This options mean
        // exclusive :  the queue is exclusive for each worker (workers dont 'compete' for the data) ;
        // autoDelete : if worker disconnects, queue is deleted (otherwise it keeps growing and growing consuming RES)

        channel.assertQueue('', {
          exclusive: true,
          autoDelete: true
        }, function(error2: any, q: any) {

          if (error2) throw error2;

          if (type === 'publish') {
            channel.bindQueue(q.queue, exchange, '');
            rabbitWorkerLog.info(`[*] Waiting for ${q.queue}. To exit press CTRL+C`, exchange);
          }

          return res({ channel, q });
        });

      });
    });

  }

  healthChecker() {

    let that = this;

    this.heartbeatInterval = setInterval(() => {

      let currentTime = new Date().getTime();

      if ( (currentTime - that.lastPingMessage) > 10000) {

        clearInterval(that.heartbeatInterval);

        // Check first if the channel can be closed (oddsChannel might have been already closed)

        that.channels.forEach((_channel: any) => {

          try {
            _channel.close();
          }
          catch (err) {
            // console.log('already closed');
          }

        });

        try {

          // Close amqp connection to start recovery process...
          that.connection.close();
        }
        catch (err) {
          // console.log('rabbit server connection already closed');
        }

      }

    }, 1000);
  }

}

const EventEmitter = require('events');
export const rabbitWorkerEmitter = new EventEmitter();

export const HAVE_PREMATCH_ACCESS = config.prematchOddsWebsocketUrl.length > 0 && config.prematchOddsUsername.length > 0 && config.prematchOddsPassword.length > 0;
export const HAVE_LIVE_ACCESS = config.liveOddsWebsocketUrl.length > 0 && config.liveOddsUsername.length > 0 && config.liveOddsPassword.length > 0;


function LastMessageLogger() {

  setInterval(() => {
    Object.keys(messageLogger)
      .forEach(logger => {
        rabbitWorkerLog.warn(`${logger} last received message ${messageLogger[logger]}`);
      });
      metricsLogger();
  }, 15000);

}

function StartRabbit() {

  // Register listeners so we can 'listen' to rabbit events
  // rabbitWorkerEmitter.on('closedConnection', (msg: any) => recoverFromDisconnect(msg));

  if (HAVE_LIVE_ACCESS) {
    let rbworkerLive = new RabbitWorker(rabbitWorkerEmitter, 'live');
    rabbitWorkerEmitter.on('updatesReceived_live', (updateObject: any) => rabbitUpdates(updateObject, 'live'));
    // rabbitWorkerEmitter.on('gamecastReceived', (gamecastData: any) => rabbitGamecastData(gamecastData));
    rabbitWorkerEmitter.on('outcomesStateReceived_live', (outcomesState: any) => rabbitOutcomesState(outcomesState, 'live'));
  }

  if (HAVE_PREMATCH_ACCESS) {
    let rbworkerPrematch = new RabbitWorker(rabbitWorkerEmitter, 'prematch');
    rabbitWorkerEmitter.on('updatesReceived_prematch', (updateObject: any) => rabbitUpdates(updateObject, 'prematch'));
    rabbitWorkerEmitter.on('outcomesStateReceived_prematch', (outcomesState: any) => rabbitOutcomesState(outcomesState, 'prematch'));
  }

  LastMessageLogger();

}

StartRabbit();
